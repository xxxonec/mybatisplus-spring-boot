package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.SessionAggrStat;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface SessionAggrStatMapper extends BaseMapper<SessionAggrStat> {

}

package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.Top10Session;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface Top10SessionMapper extends BaseMapper<Top10Session> {

}

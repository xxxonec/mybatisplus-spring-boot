package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.SessionRandomExtract;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface SessionRandomExtractMapper extends BaseMapper<SessionRandomExtract> {

}

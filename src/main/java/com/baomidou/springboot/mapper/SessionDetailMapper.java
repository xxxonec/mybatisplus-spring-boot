package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.SessionDetail;

import java.util.List;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface SessionDetailMapper extends BaseMapper<SessionDetail> {

}

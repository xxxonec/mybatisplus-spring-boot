package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.Top10Category;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface Top10CategoryMapper extends BaseMapper<Top10Category> {

}

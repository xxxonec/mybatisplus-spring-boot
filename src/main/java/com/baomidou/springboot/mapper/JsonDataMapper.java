package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.JsonData;

/**
 * Created by pcmmm on 17/6/12.
 */
public interface JsonDataMapper extends BaseMapper<JsonData> {
    JsonData selectByTaskId(Long taskid);
}

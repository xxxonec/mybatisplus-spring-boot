package com.baomidou.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springboot.entity.Task;

/**
 * Created by pcmmm on 17/6/7.
 */
public interface TaskMapper extends BaseMapper<Task> {
    void insertAndGetId(Task task);
}

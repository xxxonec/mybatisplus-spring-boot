package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.JsonData;

/**
 * Created by pcmmm on 17/6/12.
 */
public interface JsonDataService extends IService<JsonData> {
    JsonData selectByTaskId(Long taskid);
}

package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.SessionDetail;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * SessionDetail 表数据服务层接口
 *
 */
public interface SessionDetailService extends IService<SessionDetail> {

}
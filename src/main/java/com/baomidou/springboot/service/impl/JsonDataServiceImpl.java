package com.baomidou.springboot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springboot.entity.JsonData;
import com.baomidou.springboot.mapper.JsonDataMapper;
import com.baomidou.springboot.service.JsonDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pcmmm on 17/6/12.
 */
@Service
public class JsonDataServiceImpl extends ServiceImpl<JsonDataMapper,JsonData> implements JsonDataService{
    @Autowired
    JsonDataService jsonDataService;
    @Override
    public JsonData selectByTaskId(Long taskid) {
        return jsonDataService.selectByTaskId(taskid);
    }
}

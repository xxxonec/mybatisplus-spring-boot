package com.baomidou.springboot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springboot.entity.SessionDetail;
import com.baomidou.springboot.entity.User;
import com.baomidou.springboot.mapper.SessionDetailMapper;
import com.baomidou.springboot.mapper.UserMapper;
import com.baomidou.springboot.service.IUserService;
import com.baomidou.springboot.service.SessionDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * SessionDetail 表数据服务层接口实现类
 *
 */
@Service
public class SessionDetailServiceImpl extends ServiceImpl<SessionDetailMapper, SessionDetail> implements SessionDetailService {


}
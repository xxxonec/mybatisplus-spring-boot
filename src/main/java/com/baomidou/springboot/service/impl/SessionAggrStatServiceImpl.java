package com.baomidou.springboot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springboot.entity.SessionAggrStat;
import com.baomidou.springboot.entity.User;
import com.baomidou.springboot.mapper.SessionAggrStatMapper;
import com.baomidou.springboot.mapper.UserMapper;
import com.baomidou.springboot.service.IUserService;
import com.baomidou.springboot.service.SessionAggrStatService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * SessionAggrStat 表数据服务层接口实现类
 *
 */
@Service
public class SessionAggrStatServiceImpl extends ServiceImpl<SessionAggrStatMapper, SessionAggrStat> implements SessionAggrStatService {


}
package com.baomidou.springboot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springboot.entity.Top10Session;
import com.baomidou.springboot.entity.User;
import com.baomidou.springboot.mapper.Top10SessionMapper;
import com.baomidou.springboot.mapper.UserMapper;
import com.baomidou.springboot.service.IUserService;
import com.baomidou.springboot.service.Top10SessionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Top10Session 表数据服务层接口实现类
 *
 */
@Service
public class Top10SessionServiceImpl extends ServiceImpl<Top10SessionMapper, Top10Session> implements Top10SessionService {


}
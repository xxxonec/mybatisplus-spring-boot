package com.baomidou.springboot.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springboot.entity.SessionRandomExtract;
import com.baomidou.springboot.entity.User;
import com.baomidou.springboot.mapper.SessionRandomExtractMapper;
import com.baomidou.springboot.mapper.UserMapper;
import com.baomidou.springboot.service.IUserService;
import com.baomidou.springboot.service.SessionRandomExtractService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * SessionRandomExtract 表数据服务层接口实现类
 *
 */
@Service
public class SessionRandomExtractServiceImpl extends ServiceImpl<SessionRandomExtractMapper, SessionRandomExtract> implements SessionRandomExtractService {


}
package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.Task;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * Task 表数据服务层接口
 *
 */
public interface TaskService extends IService<Task> {
    void insertAndGetId(Task task);
}
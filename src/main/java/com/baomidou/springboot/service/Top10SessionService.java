package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.Top10Session;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * Top10Session 表数据服务层接口
 *
 */
public interface Top10SessionService extends IService<Top10Session> {

}
package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.Top10Category;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * Top10Category 表数据服务层接口
 *
 */
public interface Top10CategoryService extends IService<Top10Category> {

}
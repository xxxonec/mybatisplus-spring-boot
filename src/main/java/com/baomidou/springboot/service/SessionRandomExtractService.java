package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.SessionRandomExtract;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * SessionRandomExtract 表数据服务层接口
 *
 */
public interface SessionRandomExtractService extends IService<SessionRandomExtract> {

}
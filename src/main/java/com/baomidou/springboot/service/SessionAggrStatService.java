package com.baomidou.springboot.service;

import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springboot.entity.SessionAggrStat;
import com.baomidou.springboot.entity.User;

import java.util.List;

/**
 *
 * SessionAggrStat 表数据服务层接口
 *
 */
public interface SessionAggrStatService extends IService<SessionAggrStat> {

}
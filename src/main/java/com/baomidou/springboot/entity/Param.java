package com.baomidou.springboot.entity;

import java.util.List;

/**
 * Created by pcmmm on 17/6/8.
 */
public class Param {
    private List<String> startDate;
    private List<String> endDate;
    private List<String> startAge;
    private List<String> endAge;
    private List<String> cities;

    private List<String> sex;
    private List<String> searchWords;
    private List<String> professional;
    private List<String> categoryIds;

    public List<String> getStartDate() {
        return startDate;
    }

    public void setStartDate(List<String> startDate) {
        this.startDate = startDate;
    }

    public List<String> getEndDate() {
        return endDate;
    }

    public void setEndDate(List<String> endDate) {
        this.endDate = endDate;
    }

    public List<String> getStartAge() {
        return startAge;
    }

    public void setStartAge(List<String> startAge) {
        this.startAge = startAge;
    }

    public List<String> getEndAge() {
        return endAge;
    }

    public void setEndAge(List<String> endAge) {
        this.endAge = endAge;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public List<String> getSex() {
        return sex;
    }

    public void setSex(List<String> sex) {
        this.sex = sex;
    }

    public List<String> getSearchWords() {
        return searchWords;
    }

    public void setSearchWords(List<String> searchWords) {
        this.searchWords = searchWords;
    }

    public List<String> getProfessional() {
        return professional;
    }

    public void setProfessional(List<String> professional) {
        this.professional = professional;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public Param(List<String> startDate, List<String> endDate, List<String> startAge, List<String> endAge, List<String> cities, List<String> sex, List<String> searchWords, List<String> professional, List<String> categoryIds) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startAge = startAge;
        this.endAge = endAge;
        this.cities = cities;
        this.sex = sex;
        this.searchWords = searchWords;
        this.professional = professional;
        this.categoryIds = categoryIds;
    }

    public Param() {
    }
}

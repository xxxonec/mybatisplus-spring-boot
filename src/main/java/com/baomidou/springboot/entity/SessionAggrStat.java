package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * session聚合统计
 * Created by pcmmm on 17/6/7.
 */
@SuppressWarnings("serial")
@TableName("session_aggr_stat")
public class SessionAggrStat{

    @TableId("task_id")
    private long taskid;
    private long session_count;
    @TableField("1s_3s")
    private double visit_length_1s_3s_ratio;
    @TableField("4s_6s")
    private double visit_length_4s_6s_ratio;
    @TableField("7s_9s")
    private double visit_length_7s_9s_ratio;
    @TableField("10s_30s")
    private double visit_length_10s_30s_ratio;
    @TableField("30s_60s")
    private double visit_length_30s_60s_ratio;
    @TableField("1m_3m")
    private double visit_length_1m_3m_ratio;
    @TableField("3m_10m")
    private double visit_length_3m_10m_ratio;
    @TableField("10m_30m")
    private double visit_length_10m_30m_ratio;
    @TableField("30m")
    private double visit_length_30m_ratio;
    @TableField("1_3")
    private double step_length_1_3_ratio;
    @TableField("4_6")
    private double step_length_4_6_ratio;
    @TableField("7_9")
    private double step_length_7_9_ratio;
    @TableField("10_30")
    private double step_length_10_30_ratio;
    @TableField("30_60")
    private double step_length_30_60_ratio;
    @TableField("60")
    private double step_length_60_ratio;

    public long getTaskid() {
        return taskid;
    }

    public void setTaskid(long taskid) {
        this.taskid = taskid;
    }

    public long getSession_count() {
        return session_count;
    }

    public void setSession_count(long session_count) {
        this.session_count = session_count;
    }

    public double getVisit_length_1s_3s_ratio() {
        return visit_length_1s_3s_ratio;
    }

    public void setVisit_length_1s_3s_ratio(double visit_length_1s_3s_ratio) {
        this.visit_length_1s_3s_ratio = visit_length_1s_3s_ratio;
    }

    public double getVisit_length_4s_6s_ratio() {
        return visit_length_4s_6s_ratio;
    }

    public void setVisit_length_4s_6s_ratio(double visit_length_4s_6s_ratio) {
        this.visit_length_4s_6s_ratio = visit_length_4s_6s_ratio;
    }

    public double getVisit_length_7s_9s_ratio() {
        return visit_length_7s_9s_ratio;
    }

    public void setVisit_length_7s_9s_ratio(double visit_length_7s_9s_ratio) {
        this.visit_length_7s_9s_ratio = visit_length_7s_9s_ratio;
    }

    public double getVisit_length_10s_30s_ratio() {
        return visit_length_10s_30s_ratio;
    }

    public void setVisit_length_10s_30s_ratio(double visit_length_10s_30s_ratio) {
        this.visit_length_10s_30s_ratio = visit_length_10s_30s_ratio;
    }

    public double getVisit_length_30s_60s_ratio() {
        return visit_length_30s_60s_ratio;
    }

    public void setVisit_length_30s_60s_ratio(double visit_length_30s_60s_ratio) {
        this.visit_length_30s_60s_ratio = visit_length_30s_60s_ratio;
    }

    public double getVisit_length_1m_3m_ratio() {
        return visit_length_1m_3m_ratio;
    }

    public void setVisit_length_1m_3m_ratio(double visit_length_1m_3m_ratio) {
        this.visit_length_1m_3m_ratio = visit_length_1m_3m_ratio;
    }

    public double getVisit_length_3m_10m_ratio() {
        return visit_length_3m_10m_ratio;
    }

    public void setVisit_length_3m_10m_ratio(double visit_length_3m_10m_ratio) {
        this.visit_length_3m_10m_ratio = visit_length_3m_10m_ratio;
    }

    public double getVisit_length_10m_30m_ratio() {
        return visit_length_10m_30m_ratio;
    }

    public void setVisit_length_10m_30m_ratio(double visit_length_10m_30m_ratio) {
        this.visit_length_10m_30m_ratio = visit_length_10m_30m_ratio;
    }

    public double getVisit_length_30m_ratio() {
        return visit_length_30m_ratio;
    }

    public void setVisit_length_30m_ratio(double visit_length_30m_ratio) {
        this.visit_length_30m_ratio = visit_length_30m_ratio;
    }

    public double getStep_length_1_3_ratio() {
        return step_length_1_3_ratio;
    }

    public void setStep_length_1_3_ratio(double step_length_1_3_ratio) {
        this.step_length_1_3_ratio = step_length_1_3_ratio;
    }

    public double getStep_length_4_6_ratio() {
        return step_length_4_6_ratio;
    }

    public void setStep_length_4_6_ratio(double step_length_4_6_ratio) {
        this.step_length_4_6_ratio = step_length_4_6_ratio;
    }

    public double getStep_length_7_9_ratio() {
        return step_length_7_9_ratio;
    }

    public void setStep_length_7_9_ratio(double step_length_7_9_ratio) {
        this.step_length_7_9_ratio = step_length_7_9_ratio;
    }

    public double getStep_length_10_30_ratio() {
        return step_length_10_30_ratio;
    }

    public void setStep_length_10_30_ratio(double step_length_10_30_ratio) {
        this.step_length_10_30_ratio = step_length_10_30_ratio;
    }

    public double getStep_length_30_60_ratio() {
        return step_length_30_60_ratio;
    }

    public void setStep_length_30_60_ratio(double step_length_30_60_ratio) {
        this.step_length_30_60_ratio = step_length_30_60_ratio;
    }

    public double getStep_length_60_ratio() {
        return step_length_60_ratio;
    }

    public void setStep_length_60_ratio(double step_length_60_ratio) {
        this.step_length_60_ratio = step_length_60_ratio;
    }

    public SessionAggrStat(int taskid, long session_count, double visit_length_1s_3s_ratio, double visit_length_4s_6s_ratio, double visit_length_7s_9s_ratio, double visit_length_10s_30s_ratio, double visit_length_30s_60s_ratio, double visit_length_1m_3m_ratio, double visit_length_3m_10m_ratio, double visit_length_10m_30m_ratio, double visit_length_30m_ratio, double step_length_1_3_ratio, double step_length_4_6_ratio, double step_length_7_9_ratio, double step_length_10_30_ratio, double step_length_30_60_ratio, double step_length_60_ratio) {
        this.taskid = Long.valueOf(taskid);
        this.session_count = session_count;
        this.visit_length_1s_3s_ratio = visit_length_1s_3s_ratio;
        this.visit_length_4s_6s_ratio = visit_length_4s_6s_ratio;
        this.visit_length_7s_9s_ratio = visit_length_7s_9s_ratio;
        this.visit_length_10s_30s_ratio = visit_length_10s_30s_ratio;
        this.visit_length_30s_60s_ratio = visit_length_30s_60s_ratio;
        this.visit_length_1m_3m_ratio = visit_length_1m_3m_ratio;
        this.visit_length_3m_10m_ratio = visit_length_3m_10m_ratio;
        this.visit_length_10m_30m_ratio = visit_length_10m_30m_ratio;
        this.visit_length_30m_ratio = visit_length_30m_ratio;
        this.step_length_1_3_ratio = step_length_1_3_ratio;
        this.step_length_4_6_ratio = step_length_4_6_ratio;
        this.step_length_7_9_ratio = step_length_7_9_ratio;
        this.step_length_10_30_ratio = step_length_10_30_ratio;
        this.step_length_30_60_ratio = step_length_30_60_ratio;
        this.step_length_60_ratio = step_length_60_ratio;
    }

    public SessionAggrStat(long taskid, long session_count, double visit_length_1s_3s_ratio, double visit_length_4s_6s_ratio, double visit_length_7s_9s_ratio, double visit_length_10s_30s_ratio, double visit_length_30s_60s_ratio, double visit_length_1m_3m_ratio, double visit_length_3m_10m_ratio, double visit_length_10m_30m_ratio, double visit_length_30m_ratio, double step_length_1_3_ratio, double step_length_4_6_ratio, double step_length_7_9_ratio, double step_length_10_30_ratio, double step_length_30_60_ratio, double step_length_60_ratio) {
        this.taskid = taskid;
        this.session_count = session_count;
        this.visit_length_1s_3s_ratio = visit_length_1s_3s_ratio;
        this.visit_length_4s_6s_ratio = visit_length_4s_6s_ratio;
        this.visit_length_7s_9s_ratio = visit_length_7s_9s_ratio;
        this.visit_length_10s_30s_ratio = visit_length_10s_30s_ratio;
        this.visit_length_30s_60s_ratio = visit_length_30s_60s_ratio;
        this.visit_length_1m_3m_ratio = visit_length_1m_3m_ratio;
        this.visit_length_3m_10m_ratio = visit_length_3m_10m_ratio;
        this.visit_length_10m_30m_ratio = visit_length_10m_30m_ratio;
        this.visit_length_30m_ratio = visit_length_30m_ratio;
        this.step_length_1_3_ratio = step_length_1_3_ratio;
        this.step_length_4_6_ratio = step_length_4_6_ratio;
        this.step_length_7_9_ratio = step_length_7_9_ratio;
        this.step_length_10_30_ratio = step_length_10_30_ratio;
        this.step_length_30_60_ratio = step_length_30_60_ratio;
        this.step_length_60_ratio = step_length_60_ratio;
    }

    public SessionAggrStat() {
    }

    @Override
    public String toString() {
        return "SessionAggrStat{" +
                "taskid=" + taskid +
                ", session_count=" + session_count +
                ", visit_length_1s_3s_ratio=" + visit_length_1s_3s_ratio +
                ", visit_length_4s_6s_ratio=" + visit_length_4s_6s_ratio +
                ", visit_length_7s_9s_ratio=" + visit_length_7s_9s_ratio +
                ", visit_length_10s_30s_ratio=" + visit_length_10s_30s_ratio +
                ", visit_length_30s_60s_ratio=" + visit_length_30s_60s_ratio +
                ", visit_length_1m_3m_ratio=" + visit_length_1m_3m_ratio +
                ", visit_length_3m_10m_ratio=" + visit_length_3m_10m_ratio +
                ", visit_length_10m_30m_ratio=" + visit_length_10m_30m_ratio +
                ", visit_length_30m_ratio=" + visit_length_30m_ratio +
                ", step_length_1_3_ratio=" + step_length_1_3_ratio +
                ", step_length_4_6_ratio=" + step_length_4_6_ratio +
                ", step_length_7_9_ratio=" + step_length_7_9_ratio +
                ", step_length_10_30_ratio=" + step_length_10_30_ratio +
                ", step_length_30_60_ratio=" + step_length_30_60_ratio +
                ", step_length_60_ratio=" + step_length_60_ratio +
                '}';
    }
}


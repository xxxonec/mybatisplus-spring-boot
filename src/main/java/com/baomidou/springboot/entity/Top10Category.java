package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * top10品类
 * Created by pcmmm on 17/6/7.
 */
@SuppressWarnings("serial")
@Data
@TableName("top10_category")
public class Top10Category {

    @TableId("task_id")
    private long taskid;

    @TableField("category_id")
    private long categoryid;

    @TableField("click_count")
    private long clickCount;

    @TableField("order_count")
    private long orderCount;

    @TableField("pay_count")
    private long payCount;


}


package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * top10活跃session
 * Created by pcmmm on 17/6/7.
 */
@SuppressWarnings("serial")
@Data
@TableName("top10_session")
public class Top10Session {

    @TableId("task_id")
    private long taskid;

    @TableField("category_id")
    private long categoryid;

    @TableField("session_id")
    private String sessionid;

    @TableField("click_count")
    private long clickCount;

}


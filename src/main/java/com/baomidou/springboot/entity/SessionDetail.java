package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * Created by pcmmm on 17/6/7.
 */
@SuppressWarnings("serial")
@Data
@TableName("session_detail")
public class SessionDetail {

    @TableId("task_id")
    private long taskid;

    @TableField("user_id")
    private long userid;

    @TableField("session_id")
    private String sessionid;

    @TableField("page_id")
    private long pageid;

    @TableField("action_time")
    private String actionTime;

    @TableField("search_keyword")
    private String searchKeyword;

    @TableField("click_category_id")
    private long clickCategoryId;

    @TableField("click_product_id")
    private long clickProductId;

    @TableField("order_category_ids")
    private String orderCategoryIds;

    @TableField("order_product_ids")
    private String orderProductIds;

    @TableField("pay_category_ids")
    private String payCategoryIds;

    @TableField("pay_product_ids")
    private String payProductIds;



}


package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by pcmmm on 17/6/7.
 */
@TableName("task")
public class Task implements Serializable {

    private static final long serialVersionUID = 3518776796426921776L;

    @TableId(value = "task_id",type = IdType.AUTO)
    private long taskid;

    @TableField("task_name")
    private String taskName;

    @TableField("create_time")
    private String createTime;

    @TableField("start_time")
    private String startTime;

    @TableField("finish_time")
    private String finishTime;

    @TableField("task_type")
    private String taskType;

    @TableField("task_status")
    private String taskStatus;

    @TableField("task_param")
    private String taskParam;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getTaskid() {
        return taskid;
    }

    public void setTaskid(long taskid) {
        this.taskid = taskid;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskParam() {
        return taskParam;
    }

    public void setTaskParam(String taskParam) {
        this.taskParam = taskParam;
    }

    public Task(long taskid, String taskName, String createTime, String startTime, String finishTime, String taskType, String taskStatus, String taskParam) {
        this.taskid = taskid;
        this.taskName = taskName;
        this.createTime = createTime;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskParam = taskParam;
    }

    public Task(Integer taskid, String taskName, String createTime, String startTime, String finishTime, String taskType, String taskStatus, String taskParam) {
        this.taskid = Long.valueOf(taskid);
        this.taskName = taskName;
        this.createTime = createTime;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskParam = taskParam;
    }
    public Task(String taskName, String createTime, String startTime, String finishTime, String taskType, String taskStatus, String taskParam) {

        this.taskName = taskName;
        this.createTime = createTime;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskParam = taskParam;
    }

    public Task() {
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskid=" + taskid +
                ", taskName='" + taskName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", finishTime='" + finishTime + '\'' +
                ", taskType='" + taskType + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", taskParam='" + taskParam + '\'' +
                '}';
    }
}


package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * Created by pcmmm on 17/6/7.
 */
@SuppressWarnings("serial")
@Data
@TableName("session_random_extract")
public class SessionRandomExtract {

    @TableId("task_id")
    private long taskid;

    @TableField("session_id")
    private String sessionid;

    @TableField("start_time")
    private String startTime;

    @TableField("search_keywords")
    private String searchKeywords;

    @TableField("click_category_ids")
    private String clickCategoryIds;



}


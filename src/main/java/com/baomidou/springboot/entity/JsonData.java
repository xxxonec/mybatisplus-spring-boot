package com.baomidou.springboot.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * Created by pcmmm on 17/6/9.
 */
@TableName("json_data")
public class JsonData {
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("task_id")
    private Long taskid;

    @TableField("task_type")
    private String taskType;

    @TableField("data")
    private String data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getTaskid() {
        return taskid;
    }

    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public JsonData(Integer id, Long taskid, String taskType, String data) {
        this.id = id;
        this.taskid = taskid;
        this.taskType = taskType;
        this.data = data;
    }

    public JsonData() {
    }

    @Override
    public String toString() {
        return "JsonData{" +
                "id=" + id +
                ", taskid=" + taskid +
                ", taskType='" + taskType + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}

package com.baomidou.springboot.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.springboot.Application;
import com.baomidou.springboot.entity.JsonData;
import com.baomidou.springboot.entity.Param;
import com.baomidou.springboot.entity.Result;
import com.baomidou.springboot.entity.Task;
import com.baomidou.springboot.mapper.TaskMapper;
import com.baomidou.springboot.service.JsonDataService;
import com.baomidou.springboot.service.TaskService;
import com.baomidou.springboot.spark.AnalysisMain;
import com.baomidou.springboot.utils.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

import static com.baomidou.springboot.utils.ParamUtils.getParamWithSplit;

/**
 * Created by jobob on 17/5/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class AppTest {
    @Autowired
    TaskService taskService;
    @Autowired
    JsonDataService jsonDataService;
    @Test
    public void jsonTest(){
        //String jsonString = "{\"startAge\":\"26\",\"endAge\":\"40\",\"city\":[\"北京\",\"广州\",\"深圳\"],\"endDate\":\"2017-06-08\",\"sex\":[\"female\"],\"startDate\":\"2017-06-01\"}";
        String jsonString = "{\"city\":[\"北京\",\"上海\",\"广州\"],\"endAge\":50,\"searchWords\":[\"小米\",\"华为\"],\"sex\":[\"female\"],\"startAge\":20}";
        JSONObject taskParam = JSONObject.parseObject(jsonString);
        System.out.println(taskParam);
        String field = "asd";
        //System.out.println(taskParam.get("city"));
        JSONArray jsonArray = taskParam.getJSONArray(field);
        System.out.println(jsonArray.size());
    }

    @Test
    public void testTask(){
        Param param = new Param();
        List<String> city = Arrays.asList("北京","上海","广州");
        //List<String> sex = Arrays.asList("female");
        List<String> word = Arrays.asList("小米","华为");
        //param.setStartAge(20);
        //param.setEndAge(50);
        param.setCities(city);
        param.setSearchWords(word);
        //param.setSex("female");
        param.setProfessional(null);
        String paramString = JSON.toJSONString(param);
        String curDate = DateUtils.formatDate(new Date()).toString();
        Task task = new Task("task2", curDate,curDate,null,"testTask","0",paramString);
        taskService.insertAndGetId(task);
        Long id = task.getTaskid();
        System.out.println("id:"+id);
    }
    @Test
    public void test1(){
        Map map = new HashMap();
        map.put("task_id", Long.valueOf(7));
        List<JsonData> jsonDatas = jsonDataService.selectByMap(map);
        System.out.println(jsonDatas.get(0).toString());
    }

    @Test
    public void testSpark(){
        Task task = taskService.selectById(25);
        AnalysisMain.Analysis1(task);
    }


    @Test
    public void testJson(){
        Long task_id = Long.valueOf("21");
        Map sessionMap = new HashMap();
        Map cityMap = new HashMap();
        cityMap.put("task_id",task_id);
        cityMap.put("task_type","1");
        sessionMap.put("task_id", task_id);
        sessionMap.put("task_type","0");
        List<JsonData> sessionDatas = jsonDataService.selectByMap(sessionMap);
        List<JsonData> cityDatas = jsonDataService.selectByMap(cityMap);
        Map<String, Object> resultmap = JSON.parseObject(
                sessionDatas.get(0).getData(),new TypeReference<Map<String, Object>>(){} );
        Map<String, Object> resultmap1 = JSON.parseObject(
                cityDatas.get(0).getData(),new TypeReference<Map<String, Object>>(){} );
        resultmap.putAll(resultmap1);
        String jsonString = JSONObject.toJSONString(resultmap);
        System.out.println(jsonString);
        //JsonData jsonData = jsonDataService.selectById(id);
        //JSONObject result = JSONObject.parseObject(jsonString);
        //return new Result<>(result);
    }

    @Test
    public void testTime(){
        String time= "2017-06-07 3:26:08";
        String hour = DateUtils.getTimeHours(time);
        System.out.println(DateUtils.timeBetween(hour,"02:00","02:59"));

    }

}

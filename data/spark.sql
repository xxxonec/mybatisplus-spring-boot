/*
 Navicat Premium Data Transfer

 Source Server         : new
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost
 Source Database       : spark

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : utf-8

 Date: 06/15/2017 22:23:16 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `json_data`
-- ----------------------------
DROP TABLE IF EXISTS `json_data`;
CREATE TABLE `json_data` (
  `task_id` int(11) DEFAULT NULL,
  `data` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_type` varchar(255) DEFAULT NULL,
  `NewColumn1` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `json_data`
-- ----------------------------
BEGIN;
INSERT INTO `json_data` VALUES ('7', '{\"session_count\":189,\"step_length_10_30_ratio\":0.58,\"step_length_1_3_ratio\":0.09,\"step_length_30_60_ratio\":0.0,\"step_length_4_6_ratio\":0.17,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.16,\"taskid\":7,\"visit_length_10m_30m_ratio\":0.08,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.89,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.02,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '1', null, null), ('19', '{\"session_count\":756,\"step_length_10_30_ratio\":0.59,\"step_length_1_3_ratio\":0.09,\"step_length_30_60_ratio\":0.0,\"step_length_4_6_ratio\":0.15,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.17,\"taskid\":19,\"visit_length_10m_30m_ratio\":0.07,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.9,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.01,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '2', null, null), ('21', '{\"session_count\":953,\"step_length_10_30_ratio\":0.58,\"step_length_1_3_ratio\":0.1,\"step_length_30_60_ratio\":0.01,\"step_length_4_6_ratio\":0.15,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.16,\"taskid\":21,\"visit_length_10m_30m_ratio\":0.08,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.88,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.01,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '3', null, null), ('22', '{\"session_count\":336,\"step_length_10_30_ratio\":0.58,\"step_length_1_3_ratio\":0.11,\"step_length_30_60_ratio\":0.01,\"step_length_4_6_ratio\":0.16,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.15,\"taskid\":22,\"visit_length_10m_30m_ratio\":0.09,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.87,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.02,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '4', null, null), ('23', '{\"session_count\":69,\"step_length_10_30_ratio\":0.54,\"step_length_1_3_ratio\":0.12,\"step_length_30_60_ratio\":0.0,\"step_length_4_6_ratio\":0.16,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.19,\"taskid\":23,\"visit_length_10m_30m_ratio\":0.13,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.86,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.0,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '5', null, null), ('21', '{\"session_count\":953,\"step_length_10_30_ratio\":0.58,\"step_length_1_3_ratio\":0.1,\"step_length_30_60_ratio\":0.01,\"step_length_4_6_ratio\":0.15,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.16,\"taskid\":21,\"visit_length_10m_30m_ratio\":0.08,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.88,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.01,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '6', '0', null), ('21', '{\"nanjing\":169,\"hangzhou\":177,\"shanghai\":199,\"guangzhou\":100,\"shenzhen\":179,\"beijing\":129}', '7', '1', null), ('25', '{\"session_count\":953,\"step_length_10_30_ratio\":0.58,\"step_length_1_3_ratio\":0.1,\"step_length_30_60_ratio\":0.01,\"step_length_4_6_ratio\":0.15,\"step_length_60_ratio\":0.0,\"step_length_7_9_ratio\":0.16,\"taskid\":25,\"visit_length_10m_30m_ratio\":0.08,\"visit_length_10s_30s_ratio\":0.0,\"visit_length_1m_3m_ratio\":0.0,\"visit_length_1s_3s_ratio\":0.0,\"visit_length_30m_ratio\":0.88,\"visit_length_30s_60s_ratio\":0.0,\"visit_length_3m_10m_ratio\":0.01,\"visit_length_4s_6s_ratio\":0.0,\"visit_length_7s_9s_ratio\":0.0}', '15', '0', null), ('25', '{\"nanjing\":169,\"hangzhou\":177,\"shanghai\":199,\"guangzhou\":100,\"shenzhen\":179,\"beijing\":129}', '16', '1', null), ('25', '{\"t_8_9\":44,\"t_17_18\":38,\"t_18_19\":40,\"t_7_8\":52,\"t_11_12\":44,\"t_12_13\":43,\"t_6_7\":39,\"t_10_11\":42,\"t_15_16\":43,\"t_16_17\":33,\"t_5_6\":31,\"t_13_14\":46,\"t_14_15\":46,\"t_4_5\":41,\"t_20_21\":37,\"t_21_22\":48,\"t_22_23\":38,\"t_3_4\":45,\"t_2_3\":35,\"t_1_2\":44,\"t_19_20\":31,\"t_9_10\":49,\"t_0_1\":42,\"t_23_0\":0}', '17', '2', null);
COMMIT;

-- ----------------------------
--  Table structure for `session_aggr_stat`
-- ----------------------------
DROP TABLE IF EXISTS `session_aggr_stat`;
CREATE TABLE `session_aggr_stat` (
  `task_id` int(11) NOT NULL,
  `session_count` int(11) DEFAULT NULL,
  `1s_3s` double DEFAULT NULL,
  `4s_6s` double DEFAULT NULL,
  `7s_9s` double DEFAULT NULL,
  `10s_30s` double DEFAULT NULL,
  `30s_60s` double DEFAULT NULL,
  `1m_3m` double DEFAULT NULL,
  `3m_10m` double DEFAULT NULL,
  `10m_30m` double DEFAULT NULL,
  `30m` double DEFAULT NULL,
  `1_3` double DEFAULT NULL,
  `4_6` double DEFAULT NULL,
  `7_9` double DEFAULT NULL,
  `10_30` double DEFAULT NULL,
  `30_60` double DEFAULT NULL,
  `60` double DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `session_detail`
-- ----------------------------
DROP TABLE IF EXISTS `session_detail`;
CREATE TABLE `session_detail` (
  `task_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `action_time` varchar(255) DEFAULT NULL,
  `search_keyword` varchar(255) DEFAULT NULL,
  `click_category_id` int(11) DEFAULT NULL,
  `click_product_id` int(11) DEFAULT NULL,
  `order_category_ids` varchar(255) DEFAULT NULL,
  `order_product_ids` varchar(255) DEFAULT NULL,
  `pay_category_ids` varchar(255) DEFAULT NULL,
  `pay_product_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `session_random_extract`
-- ----------------------------
DROP TABLE IF EXISTS `session_random_extract`;
CREATE TABLE `session_random_extract` (
  `task_id` int(11) NOT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `search_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `task`
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(255) DEFAULT NULL,
  `create_time` varchar(255) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `finish_time` varchar(255) DEFAULT NULL,
  `task_type` varchar(255) DEFAULT NULL,
  `task_status` varchar(255) DEFAULT NULL,
  `task_param` text,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `task`
-- ----------------------------
BEGIN;
INSERT INTO `task` VALUES ('2', 'task2', '2017-06-08', '2017-06-08', null, 'testTask', '0', '{\"city\":[\"北京\",\"上海\",\"广州\"],\"endAge\":50,\"searchWords\":[\"小米\",\"华为\"],\"sex\":[\"female\"],\"startAge\":20}'), ('3', 'task2', '2017-06-08', '2017-06-08', null, 'testTask', '0', '{\"city\":[\"北京\",\"上海\",\"广州\"],\"endAge\":50,\"searchWords\":[\"小米\",\"华为\"],\"sex\":[\"female\"],\"startAge\":20}'), ('4', 'task1', '2017-06-12', '2017-06-12', null, 'testTask', '0', '{\"city\":[\"北京\",\"广州\",\"深圳\"],\"endAge\":40,\"endDate\":\"2017-06-08\",\"sex\":[\"female\"],\"startAge\":26,\"startDate\":\"2017-06-01\"}'), ('5', 'task112', '2017-06-12', '2017-06-12', null, 'testTask', '0', '{\"city\":[\"北京\",\"广州\",\"深圳\"],\"endAge\":\"40\",\"endDate\":\"2017-06-09\",\"sex\":[\"male\"],\"startAge\":\"30\",\"startDate\":\"2017-06-01\"}'), ('6', 'task5', '2017-06-12', '2017-06-12', null, 'testTask', '0', '{\"city\":[\"北京\",\"广州\",\"南京\"],\"endAge\":\"63\",\"endDate\":\"2017-06-08\",\"sex\":[\"male\"],\"startAge\":\"23\",\"startDate\":\"2017-06-05\"}'), ('7', 'task5', '2017-06-12', '2017-06-12', null, 'testTask', '1', '{\"cities\":[\"北京\",\"广州\",\"深圳\"],\"endAge\":[\"61\"],\"endDate\":[\"2017-06-09\"],\"sex\":[\"male\"],\"startAge\":[\"24\"],\"startDate\":[\"2017-06-05\"]}'), ('8', 'task1', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"上海\",\"广州\",\"深圳\"],\"endAge\":[\"40\"],\"endDate\":[\"2017-06-09\"],\"sex\":[\"male\"],\"startAge\":[\"30\"],\"startDate\":[\"2017-06-01\"]}'), ('9', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('10', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('11', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('12', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('13', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('14', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('15', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('16', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('17', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('18', 'task2', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\"],\"searchWords\":[\"小米\",\"华为\"]}'), ('19', 'task8', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\",\"深圳\",\"南京\"],\"endAge\":[\"62\"],\"endDate\":[\"2017-06-13\"],\"startAge\":[\"22\"],\"startDate\":[\"2017-06-01\"]}'), ('20', 'taskxx', '2017-06-13', '2017-06-13', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\",\"深圳\",\"南京\",\"杭州\"],\"endAge\":[\"70\"],\"endDate\":[\"2017-06-12\"],\"startAge\":[\"18\"],\"startDate\":[\"2017-06-01\"]}'), ('21', 'task10', '2017-06-14', '2017-06-14', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\",\"深圳\",\"南京\",\"杭州\"],\"endAge\":[\"70\"],\"endDate\":[\"2017-06-14\"],\"startAge\":[\"18\"],\"startDate\":[\"2017-06-01\"]}'), ('22', 'task11', '2017-06-14', '2017-06-14', null, 'testTask', '0', '{\"cities\":[\"广州\",\"深圳\",\"南京\",\"杭州\"],\"endAge\":[\"70\"],\"endDate\":[\"2017-06-14\"],\"sex\":[\"male\"],\"startAge\":[\"19\"],\"startDate\":[\"2017-06-01\"]}'), ('23', 'qweqwe', '2017-06-14', '2017-06-14', null, 'testTask', '0', '{\"cities\":[\"北京\"],\"endAge\":[\"40\"],\"endDate\":[\"2017-06-13\"],\"sex\":[\"male\"],\"startAge\":[\"30\"],\"startDate\":[\"2017-06-01\"]}'), ('24', 'taskkk', '2017-06-15', '2017-06-15', null, 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\",\"深圳\",\"南京\",\"杭州\"],\"endAge\":[\"70\"],\"endDate\":[\"2017-06-15\"],\"startAge\":[\"18\"],\"startDate\":[\"2017-06-01\"]}'), ('25', 'task101', '2017-06-15', '2017-06-15 16:59:27', '2017-06-15 17:00:32', 'testTask', '0', '{\"cities\":[\"北京\",\"上海\",\"广州\",\"深圳\",\"南京\",\"杭州\"],\"endAge\":[\"70\"],\"endDate\":[\"2017-06-15\"],\"startAge\":[\"18\"],\"startDate\":[\"2017-06-01\"]}');
COMMIT;

-- ----------------------------
--  Table structure for `top10_category`
-- ----------------------------
DROP TABLE IF EXISTS `top10_category`;
CREATE TABLE `top10_category` (
  `task_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `click_count` int(11) DEFAULT NULL,
  `order_count` int(11) DEFAULT NULL,
  `pay_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `top10_category_session`
-- ----------------------------
DROP TABLE IF EXISTS `top10_category_session`;
CREATE TABLE `top10_category_session` (
  `task_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `click_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
